import { oneWay } from '@ember/object/computed';
import { inject as service } from '@ember/service';
import Controller from '@ember/controller';
import { run } from '@ember/runloop';
import { get, set, observer } from '@ember/object';
import config from '../config/environment';

export default Controller.extend({
  cookies:           service(),
  settings:          service(),

  resourceActions:   service('resource-actions'),
  tooltipService:    service('tooltip'),
  router:            service(),

  // GitHub auth params
  queryParams:       ['isPopup', 'fromAuthProvider'],

  error:             null,
  error_description: null,
  state:             null,
  code:              null,
  isPopup:           null,
  uToken:            '',
  userInfo:          {
    'id':             null,
    'name':           '',
    'projCode':       null,
    'projId':         null,
    'projName':       '',
    'role':           null,
    'roleIcon':       '',
  },
  projRows:            [],
  menu1st:             [],
  menu2nd:             [],
  menu3rd:             [],
  expandAll:     false,

  tooltip:           oneWay('tooltipService.tooltipOpts.type'),
  tooltipTemplate:   oneWay('tooltipService.tooltipOpts.template'),

  init() {
    this._super(...arguments);

    const cookies = get(this, 'cookies');
    let token = cookies.get('U-TOKEN');
    let projId = cookies.get('PROJECT-ID');
    let projCode = cookies.get('PROJECT-CODE');

    this.set('uToken', token);
    this.set('userInfo.projId', projId);
    this.set('userInfo.projCode', projCode);
    this.getUserInfo();
    this.getProjectsInfo();
    if ( this.get('app.environment') === 'development' ) {
      run.backburner.DEBUG = true;
    }
  },


  actions: {
    toggleExpand() {
      this.toggleProperty('expandAll');
    },

    toProjDetail(projId){
      if ( projId !== get(this, 'userInfo.projId') ){
        window.location.href = `${ config.APP.webBaseUrl }/#/comm/projects/projectDetail/${  projId }` ;
      }
    },

    downloadGuide(){
      let ifr = document.createElement('iframe');

      ifr.style.display = 'none';
      document.body.appendChild(ifr);
      ifr.src = '/assets/sample/guide_and_sample.zip';
      ifr.onload = function(){
        document.body.removeChild(ifr);
        ifr = null;
      };
    },

    moveToMenu(url){
      if (!( url.startsWith( 'https:' ) ||  url === '' || url === null ) ){
        window.location.href = `${ config.APP.webBaseUrl }${  url }` ;
      }
    },

  },

  // currentRouteName is set by Ember.Router
  // but getting the application controller to get it is inconvenient sometimes
  currentRouteNameChanged: observer('router.currentRouteName', function() {
    this.set('app.currentRouteName', this.get('router.currentRouteName'));
  }),

  ajaxPromise(type, url, data){
    data = data === null ? null : JSON.stringify(data);

    return new Promise(((resolve, reject) => {
      $.ajax({
        type,
        url,
        data,
        async:       false,
        contentType: 'application/json'
      })
        .done((xhr) => {
          resolve( xhr);
        })
        .fail((xhr, status, /* errorThrown */ ) => {
          console.log(status);
          reject( 'error');
        });
    }));
  },

  getUserInfo(){
    this.ajaxPromise('get', `${ config.APP.apiBaseUrl }/comm-api/api/hubpop/v1/auth?token=${  get(this, 'uToken') }`, null).then( (data) => {
      set(this, 'userInfo.id', data.id);
      set(this, 'userInfo.name', data.name);
      set(this, 'userInfo.role', data.role);

      if ( data.role === 'Manager' ){
        set(this, 'userInfo.roleIcon', 'userIcon03');
      }

      this.getMenuInfo();
    }).catch( () => {

    });
  },

  getProjectsInfo(){
    this.ajaxPromise('get', `${ config.APP.apiBaseUrl }/comm-api/api/hubpop/v1/projects?token=${  get(this, 'uToken') }`, null).then( (data) => {
      let proj = data.filter((project) => {
        return project.id.toString() === get(this, 'userInfo.projId');
      });

      if (proj.length >= 1){
        set(this, 'userInfo.projName', proj[0].name);
      }

      let projList = data.filter((project, index) => {
        if (project.id.toString() === get(this, 'userInfo.projId')){
          project['class'] = 'dept2 ng-binding on';
        } else {
          project['class'] = 'dept2 ng-binding';
        }

        return index < 5;
      });

      set(this, 'projRows', projList);
    }).catch( () => {

    });
  },

  getMenuInfo(){
    this.ajaxPromise('get', `${ config.APP.apiBaseUrl }/comm-api/api/hubpop/v1/menus`, null).then( (data) => {
      const userRole = get(this, 'userInfo.role');
      let menus = data.items.filter((menu) => {
        if ( userRole === 'Manager' ){
          return menu.isManager === true;
        } else if (userRole === 'UserPM'){
          return menu.isUserPm === true;
        } else if (userRole === 'User'){
          return menu.isUser === true;
        }
      });

      set(this, 'menu3rd', menus.filter((menu) => {
        return menu.level === 3;
      }));

      set(this, 'menu2nd', menus.filter((menu) => {
        return menu.level === 2;
      }));

      set(this, 'menu1st', menus.filter((menu) => {
        if (menu.serviceName.startsWith( '쿠버네티스' )){
          menu['class'] = 'dept1';
        } else {
          menu['class'] = 'dept1';
        }

        return menu.level === 1;
      }));
    }).catch( () => {

    });
  },
});
