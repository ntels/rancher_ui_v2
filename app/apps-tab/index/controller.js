import { alias } from '@ember/object/computed';
import { inject as service } from '@ember/service';
import Controller, { inject as controller } from '@ember/controller';
import C from 'ui/utils/constants';
import { computed, get, observer } from '@ember/object';
import { once } from '@ember/runloop';
import { filter } from 'ui/utils/search-text';

export default Controller.extend({
  prefs:             service(),
  intl:              service(),
  catalog:           service(),
  hubpop:            service('hubpop-service'),
  projectController: controller('authenticated.project'),

  sortBy:            'name',

  headers: [
    {
      name:           'state',
      sort:           ['sortState', 'name', 'id'],
      type:           'string',
      searchField:    'displayState',
      translationKey: 'generic.state',
      width:          125,
    },
    {
      name:           'name',
      sort:           ['name', 'id'],
      translationKey: 'generic.name',
    },
    {
      name:           'version',
      sort:           ['version', 'name', 'id'],
      translationKey: 'generic.version',
    },
    {
      name:           'endpoint',
      sort:           ['endpoint', 'name', 'id'],
      searchField:    'displayEndpoints',
      translationKey: 'appDetailPage.endpoints.endpoint',
    },
    {
      name:           'scale',
      sort:           ['scale:desc', 'isGlobalScale:desc', 'displayName'],
      searchField:    null,
      translationKey: 'stacksPage.table.scale',
      classNames:     'text-center',
      width:          100
    },
    {
      classNames:     'text-right pr-20',
      name:           'created',
      translationKey: 'generic.created',
      sort:           ['created:desc', 'name', 'id'],
      searchField:    false,
      type:           'string',
      width:          150,
    },
  ],

  tags:              alias('projectController.tags'),
  templatesObsvr: observer('model.apps.[]', function() {
    once(() => this.get('catalog').fetchAppTemplates(get(this, 'model.apps')));
  }),

  filteredApps: computed('model.apps.@each.{type,isFromCatalog,tags,state}', 'tags', 'searchText', function() {
    var needTags = get(this, 'tags');

    var apps = get(this, 'model.apps').filter((ns) => !C.REMOVEDISH_STATES.includes(get(ns, 'state')));

    if ( needTags && needTags.length ) {
      apps = apps.filter((obj) => obj.hasTags(needTags));
    }

    apps = apps.filterBy('isIstio', false);
    apps = apps.sortBy('displayName');

    const { matches } = filter(apps, get(this, 'searchText'));

    const group = [];
    let dataIndex = 0;

    matches.forEach((app, index) => {
      if ( index % 2 === 0 ) {
        group.push([app]);
        dataIndex++;
      } else {
        group[dataIndex - 1].push(app);
      }
    });

    return group;
  }),

  apps: computed('model.apps.@each.{type,isFromCatalog,tags,state}', 'tags', 'searchText', function() {
    var needTags = get(this, 'tags');

    var apps = get(this, 'model.apps').filter((ns) => !C.REMOVEDISH_STATES.includes(get(ns, 'state')));

    if ( needTags && needTags.length ) {
      apps = apps.filter((obj) => obj.hasTags(needTags));
    }

    apps = apps.filterBy('isIstio', false);
    apps = apps.sortBy('displayName');

    const { matches } = filter(apps, get(this, 'searchText'));

    const group = [];
    let dataIndex = 0;

    matches.forEach((app, index) => {
      if ( index % 2 === 0 ) {
        group.push([app]);
        dataIndex++;
      } else {
        group[dataIndex - 1].push(app);
      }
    });

    return apps;
  }),
});
