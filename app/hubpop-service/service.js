import Service from '@ember/service';
import { inject as service } from '@ember/service';

export default Service.extend({
  store:          service('store'),
  cardUse:        false,

  init(){
    this._super(...arguments);
  },
});