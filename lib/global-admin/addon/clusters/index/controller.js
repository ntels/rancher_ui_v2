import { inject as service } from '@ember/service';
import Controller, { inject as controller } from '@ember/controller';
import { getOwner } from '@ember/application';
import { get, computed } from '@ember/object';
import { filter } from 'ui/utils/search-text';

const headers = [
  {
    name:           'provider',
    searchField:    'displayProvider',
    sort:           ['displayProvider', 'name', 'id'],
    translationKey: 'clustersPage.provider.label',
  },
  {
    name:           'name',
    searchField:    'displayName',
    sort:           ['displayName', 'id'],
    translationKey: 'clustersPage.cluster.label',
  },
  {
    name:           'project',
    translationKey: 'clustersPage.project.label',
  },
  {
    name:           'state',
    sort:           ['sortState', 'name', 'id'],
    translationKey: 'generic.state',
  },
  {
    name:           'resource',
    searchField:    'nodes.length',
    sort:           ['nodes.length', 'name', 'id'],
    translationKey: 'clustersPage.resource.label',
  },
  {
    classNames:     'text-right pr-20',
    name:           'created',
    sort:           ['created', 'id'],
    searchField:    false,
    translationKey: 'clustersPage.created.label',
    width:          180,
  },
];

const NODE_SEARCH_FIELDS = ['displayName', 'externalIpAddress:ip', 'ipAddress:ip'];

export default Controller.extend({
  modalService:       service('modal'),
  access:             service(),
  scope:              service(),
  settings:           service(),
  prefs:              service(),
  router:             service(),
  cookies:            service(),
  hubpop:             service('hubpop-service'),

  application:        controller(),
  queryParams:        ['mode'],
  mode:               'grouped',

  headers,
  extraSearchFields:  ['version.gitVersion'],
  sortBy:             'name',
  searchText:         null,
  bulkActions:        false,

  extraSearchSubFields: NODE_SEARCH_FIELDS,
  psArray:              [],
  publicClusterName:    'msa2',

  actions: {
    launchOnCluster(model) {
      let authenticated = getOwner(this).lookup('route:authenticated');

      if (this.get('scope.currentProject.id') === model.get('defaultProject.id')) {
        this.transitionToRoute('authenticated.host-templates', {
          queryParams: {
            clusterId: model.get('id'),
            backTo:    'clusters'
          }
        });
      } else {
        authenticated.send('switchProject', model.get('defaultProject.id'), 'authenticated.host-templates', [model.id, { queryParams: { backTo: 'clusters' } }]);
      }
    },
    useKubernetes(model) {
      let authenticated = getOwner(this).lookup('route:authenticated');

      authenticated.send('switchProject', model.get('defaultProject.id'), 'authenticated.cluster.import', [model.id, { queryParams: { backTo: 'clusters' } }]);
    },
  },

  projectId: computed('cookies', function() {
    const cookies = get(this, 'cookies');
    const projectId = cookies.get('PROJECT-ID');

    console.log('projectId', projectId);

    return projectId;
  }),

  rows: computed('model.clusters.[]', function() {
    const privateClusters = get(this, 'privateClusters');
    const publicProjects = get(this, 'publicProjects');

    const out = [].concat(privateClusters, publicProjects);

    console.log('privateClusters', out);

    return out;
  }),

  privateClusters: computed('model.clusters.[]', function() {
    const projectId = get(this, 'projectId');

    const { matches } =  filter(get(this, 'model.clusters').slice(), projectId, ['displayName']);

    return matches;
  }),

  publicCluster: computed('model.clusters.[]', function() {
    return get(this, 'model.clusters').findBy('name', get(this, 'publicClusterName'));
  }),

  publicProjects: computed('model.clusters.[]', function() {
    const projectId = get(this, 'projectId');
    const publicCluster = get(this, 'publicCluster');

    const { matches } =  filter((publicCluster.projects).slice(), projectId, ['displayName']);

    return matches;
  }),

});
