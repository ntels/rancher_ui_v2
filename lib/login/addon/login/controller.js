import $ from 'jquery';
import { later, schedule } from '@ember/runloop';
import {
  get, set, computed, observer, setProperties
} from '@ember/object';
import { equal, alias } from '@ember/object/computed';
import { inject as service } from '@ember/service';
import Controller from '@ember/controller';
import { on } from '@ember/object/evented';
import C from 'ui/utils/constants';
import { isEmpty } from '@ember/utils';
import { next } from '@ember/runloop';
import config from 'ui/config/environment';

const USER_PASS_PROVIDERS = ['local', 'activedirectory', 'openldap', 'freeipa'];

export default Controller.extend({
  access:              service(),
  cookies:             service(),
  settings:            service(),
  intl:                service(),
  globalStore:         service(),
  modalService:        service('modal'),
  router:              service(),
  session:             service(),

  queryParams:         ['errorMsg', 'resetPassword', 'errorCode'],
  waiting:             false,
  adWaiting:           false,
  localWaiting:        false,
  shibbolethWaiting:   false,
  errorMsg:            null,
  errorCode:           null,
  resetPassword:       false,
  code:                null,
  showLocal:           null,

  adminInfo:      {
    'id':  config.adminId,
    'pwd': config.adminPwd
  },
  projectName:    null,
  projectId:      null,
  prefix:         'hubpop',
  passwordPrefix: 'ntels#1$',

  createUserInfo: {
    'username':           '',
    'password':           '',
    'enabled':            true,
    'me':                 false,
    'mustChangePassword': false,
    'type':               'user'
  },

  createGlobalRoleBindingInfo: {
    'userId':       '',
    'type':         'globalRoleBinding',
    'globalRoleId': 'user',
    'subjectKind':  'User'
  },

  globalRoleIdList: ['user-base', 'clusters-create', 'catalogs-manage', 'catalogs-use'],

  promptPasswordReset: alias('resetPassword'),
  isForbidden:         equal('errorCode', '403'),

  init() {
    this._super(...arguments);
    this.createUser();
  },

  actions: {
    started() {
      setProperties(this, {
        'waiting':  true,
        'errorMsg': null,
      });
    },

    waiting(provider) {
      set(this, 'errorMsg', null);

      switch (provider) {
      case 'local':
        this.toggleProperty('localWaiting');
        break;
      case 'activedirectory':
      case 'openldap':
      case 'freeipa':
        this.toggleProperty('adWaiting');
        break;
      case 'azuread':
        this.toggleProperty('azureadWaiting');
        break;
      case 'shibboleth':
        this.toggleProperty('shibbolethWaiting');
        break;
      default:
        break;
      }
    },

    complete(success) {
      if (success) {
        this.shouldSetServerUrl().then((proceed) => {
          if (proceed) {
            this.send('finishComplete');
          } else {
            get(this, 'router').transitionTo('update-critical-settings');
          }
        });
      }
    },

    finishComplete() {
      set(this, 'code', null);

      const redirectURL = get(this, `session.${ C.SESSION.BACK_TO }`);

      if ( this.shouldRedirect(redirectURL) ) {
        window.location.href = redirectURL;
      } else {
        get(this, 'router').replaceWith('authenticated');
      }
    },

    authenticate(provider, code) {
      this.send('waiting', provider);

      later(() => {
        get(this, 'access').login(provider, code).then((user) => {
          if ( get(user, 'mustChangePassword') && provider === 'local' ) {
            get(this, 'session').set(C.SESSION.BACK_TO, window.location.origin);
            get(this, 'access').set('userCode', code);
            get(this, 'router').transitionTo('update-password');
          } else {
            setProperties(this, {
              user: null,
              code: null,
            });
            get(this, 'access').set('userCode', null);
            get(this, 'access').set('firstLogin', false);
            this.send('complete', true);
            this.send('waiting', provider);
          }
        }).catch((err) => {
          this.send('waiting', provider);

          if (err) {
            let key;

            if ( [401, 403].includes(err.status) ) {
              key = 'authFailed'

              if ( USER_PASS_PROVIDERS.includes(provider) ) {
                key = 'authFailedCreds';
              }
            } else {
              key = 'unknown';

              if (this.intl.exists(`loginPage.error.${ err.message }`)) {
                key = err.message;
              }
            }

            set(this, 'errorMsg', key);
          }
        });
      }, 10);
    },

    toggleAuth() {
      this.toggleProperty('showLocal');
      next(this, 'focusSomething');
    },
  },

  initErrorChanged: observer('app.initError', function() {
    this.focusSomething(); // focus the button..
  }),

  isGithub: computed('access.providers', function() {
    return !!get(this, 'access.providers').findBy('id', 'github');
  }),

  isGoogle: computed('access.providers', function() {
    return !!get(this, 'access.providers').findBy('id', 'googleoauth');
  }),

  isPing: computed('access.providers', function() {
    return !!get(this, 'access.providers').findBy('id', 'ping');
  }),

  isKeycloak: computed('access.providers', function() {
    return !!get(this, 'access.providers').findBy('id', 'keycloak');
  }),

  isAdfs: computed('access.providers', function() {
    return !!get(this, 'access.providers').findBy('id', 'adfs');
  }),

  isOkta: computed('access.providers', function() {
    return !!get(this, 'access.providers').findBy('id', 'okta');
  }),

  isActiveDirectory: computed('access.provider', function() {
    return !!get(this, 'access.providers').findBy('id', 'activedirectory');
  }),

  isOpenLdap: computed('access.provider',  function() {
    return !!get(this, 'access.providers').findBy('id', 'openldap');
  }),

  isFreeIpa: computed('access.provider',  function() {
    return !!get(this, 'access.providers').findBy('id', 'freeipa');
  }),

  isLocal: computed('access.providers', function() {
    return !!get(this, 'access.providers').findBy('id', 'local');
  }),

  onlyLocal: computed('access.providers.@each.id', function() {
    const providers = (get(this, 'access.providers') || []).filter((x) => x.id !== 'local');

    return get(providers, 'length') === 0;
  }),

  externalProvider: computed('access.providers.@each.id', function() {
    const providers = (get(this, 'access.providers') || []).filter((x) => x.id !== 'local');

    return providers[0] && providers[0].id;
  }),

  isAzureAd: computed('access.provider', function() {
    return !!get(this, 'access.providers').findBy('id', 'azuread');
  }),

  isShibboleth: computed('access.provider', function() {
    return !!get(this, 'access.providers').findBy('id', 'shibboleth');
  }),

  bootstrap: on('init', function() {
    schedule('afterRender', this, 'focusSomething');
  }),

  infoMsg: computed('errorMsg', 'errorCode', 'intl.locale', function() {
    let { errorMsg: errorMessageKey } = this;


    if ( errorMessageKey ) {
      return this.intl.t(`loginPage.error.${ errorMessageKey }`);
    } else if ( get(this, 'isForbidden') ) {
      return this.intl.t('loginPage.error.authFailed');
    } else {
      return '';
    }
  }),

  createUser(){
    const cookies = get(this, 'cookies');
    let projId = cookies.get('PROJECT-ID');
    let userName = `${ this.prefix }${  projId }`;
    let password = `${ this.passwordPrefix }${  projId }`;
    const provider = 'local';
    const code = {
      username: this.adminInfo.id
      , password: this.adminInfo.pwd
    };

    if (projId === null){
      return;
    }

    later(() => {
      get(this, 'access').login(provider, code).then((/* user */) => {
        setProperties(this, {
          user: null,
          code: null,
        });
        get(this, 'access').set('userCode', null);
        get(this, 'access').set('firstLogin', false);
        this.send('complete', true);
        this.send('waiting', provider);

        this.ajaxPromise('get', `/v3/users?username=${ userName }`, null).then( (xhr) => {
          if (xhr.data !== null && xhr.data.length >= 1) {
            set(this, 'userId', xhr.data[0].id);
            set(this, 'principalIds', xhr.data[0].principalIds);
            this.hubpopLogin();
          } else {
            set(this, 'createUserInfo.username', userName);
            set(this, 'createUserInfo.password', password);

            this.ajaxPromise('post', '/v3/users', get(this, 'createUserInfo')).then( (xhr) => {
              set(this, 'userId', xhr.id);
              set(this, 'principalIds', xhr.principalIds);
              this.createGlobalRoleBinding(this.getNextGlobalRole(null));
            }).catch( () => {
              this.hubpopLogout();
            });
          }
        }).catch( () => {
          this.hubpopLogout();
        });
      }).catch((err) => {
        console.log(`err${  err }`);
        this.send('waiting', provider);
        this.hubpopLogout();
      });
    }, 10);
  },

  hubpopLogin(){
    this.hubpopLogout();

    const cookies = get(this, 'cookies');
    let projId = cookies.get('PROJECT-ID');
    let userName = `${ this.prefix }${  projId }`;
    let userPassword = `${ this.passwordPrefix }${  projId }`;
    const provider = 'local';
    const code = {
      username: userName
      , password: userPassword
    };

    later(() => {
      get(this, 'access').login(provider, code).then((/* user */ ) => {
        setProperties(this, {
          user: null,
          code: null,
        });
        get(this, 'access').set('userCode', null);
        get(this, 'access').set('firstLogin', false);
        this.send('complete', true);
        this.send('waiting', provider);
      }).catch(( err ) => {
        console.log(`err${  err }`);
        this.hubpopLogout();
      });
    }, 10);
  },

  hubpopLogout() {
    let access = get(this, 'access');

    access.clearToken().finally(() => {
      get(this, 'tab-session').clear();
      set(this, `session.${ C.SESSION.CONTAINER_ROUTE }`, undefined);
      set(this, `session.${ C.SESSION.ISTIO_ROUTE }`, undefined);
      set(this, `session.${ C.SESSION.CLUSTER_ROUTE }`, undefined);
      set(this, `session.${ C.SESSION.PROJECT_ROUTE }`, undefined);
    });
  },

  ajaxPromise(type, url, data){
    // eslint-disable-next-line no-undef
    const cookies = get(this, 'cookies');
    let csrf = cookies.get('CSRF');

    data = data === null ? null : JSON.stringify(data);

    return new Promise(((resolve, reject) => {
      $.ajax({
        type,
        url,
        data,
        async:   false,
        headers: {
          'x-api-action-links': 'actionLinks'
          , 'x-api-csrf':         csrf
          , 'x-api-no-challenge': true
          , 'Accept':             'application/json'
        },
        contentType: 'application/json'
      })
        .done((xhr) => {
          resolve( xhr);
        })
        .fail((xhr, status, /* errorThrown */ ) => {
          console.log(status);
          reject( 'error');
        });
    }));
  },

  createGlobalRoleBinding(role){
    let userId = get(this, 'userId');

    if (userId === null || role === null){
      this.failProjectProc();

      return;
    }

    set(this, 'createGlobalRoleBindingInfo.userId', userId);
    set(this, 'createGlobalRoleBindingInfo.globalRoleId', role);
    this.ajaxPromise('post', '/v3/globalrolebinding', get(this, 'createGlobalRoleBindingInfo')).then( (xhr) => {
      let nextRole = this.getNextGlobalRole(xhr.globalRoleId);

      if (nextRole === null) {
        this.hubpopLogin();
      } else {
        this.createGlobalRoleBinding(nextRole);
      }
    }).catch( () => {
      this.failProjectProc();
    });
  },

  getNextGlobalRole(role) {
    return this.getNextListItem('globalRoleIdList', role)
  },

  getNextListItem(listItem, item) {
    let list = get(this, listItem);

    if (item === null) {
      return list[0];
    } else {
      for (let i = 0; i < list.length; i++) {
        if (item === list[i]) {
          if (i + 1 < list.length){
            return list[i + 1];
          }
        }
      }
    }

    return null;
  },

  shouldSetServerUrl() {
    // setting isn't loaded yet
    let globalStore = get(this, 'globalStore');

    return globalStore.find('setting', C.SETTING.SERVER_URL).then((serverUrl) => {
      if (serverUrl && isEmpty(get(serverUrl, 'value')) && get(serverUrl, 'links.update')) {
        return false;
      }

      return true;
    });
  },

  focusSomething() {
    var user = $('.login-user')[0];
    var pass = $('.login-pass')[0];

    if ( user ) {
      if ( user.value ) {
        pass.focus();
      } else {
        user.focus();
      }
    }
  },

  shouldRedirect(redirect) {
    if ( !redirect ) {
      return false;
    }

    const current = `${ window.location.origin }${ window.location.pathname }` ;

    if ( current === redirect || `${ rootUrl }/` === redirect ) {
      return false;
    }

    const rootUrl = current.substr(0, current.length - 6);

    if ( redirect.startsWith(rootUrl) && redirect !== rootUrl && redirect !== `${ rootUrl }/` ) {
      return true;
    }

    return false;
  },

  isInsecure: window.location.protocol === 'http:',

});
