import { helper } from '@ember/component/helper';
import { isEmpty } from '@ember/utils';

export default helper(([obj]) => {
  return isEmpty(obj);
});