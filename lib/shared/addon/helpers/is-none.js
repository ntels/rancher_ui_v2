import { helper } from '@ember/component/helper';
import { isNone } from '@ember/utils';

export default helper(([obj]) => {
  return isNone(obj);
});