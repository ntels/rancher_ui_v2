/******** html5의 input type date및 month 속성을 jq datepicker로 바꾸기 워한 type속성 변경 스크립트 *******/
function html5DateMonthTypeChange() {
	$('input[type=date]').each(function() {
		$(this).attr('type','text').addClass('datepicker').attr("readonly",true);
	});
}

/******** dateMonthPicker 스크립트 *******/
function dateMonthPicker() {
	//$('#ui-datepicker-div').remove();
	$( "input.datepicker" ).attr("readonly",true).next('span.inline').remove();	

	//multi From Date Minimum To Date Maximum setting// class를 이용한 방법 multi setting 때문 id로 작업시 중복 id는 구현되지 않음
	// From Date Minimum setting		
	$( ".datepicker.startDate" ).datepicker({
		onClose: function( selectedDate ) {
			$( ".endDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		
	// To Date Maximum setting
	$( ".datepicker.endDate" ).datepicker({
	});
	
	// Minimum Maximum not setting 인 경우
	$( ".datepicker" ).datepicker({
	});
}



function lnbAction() { //좌메뉴 예시입니다. 실제 개발계에 붙이실때는 해당부분 삭제바랍니다.
	$(document).on('click','.gnbMenu a.dept1', function() {
		if($(this).hasClass('active')) {
			if($(this).next('ul.dept2').is(':visible')) {
				$(this).next('ul.dept2').slideUp('fast');
			} else {
				$('.gnbMenu a.dept1').not('.active').removeClass('on');
				$('.gnbMenu ul.dept2').not(this).slideUp('fast');
				$(this).next('ul.dept2').slideDown('fast');
			};
		} else {
			if($(this).hasClass('on')) {
				$(this).removeClass('on');
				$(this).next('ul.dept2').slideUp('fast');
			} else {
				$('.gnbMenu a.dept1').not('.active').removeClass('on');
				$(this).addClass('on');
				$('.gnbMenu ul.dept2').not(this).slideUp('fast');
				$(this).next('ul.dept2').slideDown('fast');
			};
		};
	});
	$(document).on('mouseleave','.gnbMenu ul.dept3, .dept1',function() {
		$(this).prev('a').removeClass('on open');
		$('.gnbMenu ul.dept3').hide();
	});
};

// 19Y ui 관련 ie 대응
function inlineEllipsis() {
	var _agent = navigator.userAgent.toLowerCase();
	if ( (navigator.appName == 'Netscape' && navigator.userAgent.search('Trident') != -1) || (_agent.indexOf("msie") != -1) ) { //crossbrowsing width 관련 대응
		$.each($('.ncz_dot_lst.inline > li'),function() {
			$(this).closest('.ncz_dot_lst').addClass('ie');
			var tit = $(this).children('strong');
			var cnt = $(this).children('span');
			$(this).css({'text-overflow':'clip','white-space':'normal'});
			cnt.width($(this).outerWidth() - (tit.outerWidth() + parseInt(tit.css('padding-left').replace(/[^0-9]/g, "")) + 4));

			$(this).find('.copy').css({'max-width':$(this).find('.copy').outerWidth(),'width':'auto'});
		});
	};
};

$(document).one('ready',function(event){  
	event.stopPropagation();
	html5DateMonthTypeChange();
	dateMonthPicker();
	lnbAction();
	
	inlineEllipsis();
});